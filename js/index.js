((global, Vue) => {
    let vueWidget = document.querySelector('.vue');
    function  initVue() {
        let vm = new Vue({
            el: '.vue',
            data: {
                data: "",
            },
            methods: {
                clickHandler: function (event) {
                    global.location = 'xeninfo:openurl:google.com/search?q=' + this.data.split(' ').join('+');
                    this.data = '';
                }
            }
        });
    }
    global.addEventListener("load", function() {
        let input = document.querySelector('.input');
        let btn = document.querySelector('button');

        document.body.style.width='100%';
        document.body.style.height='100%';

        input.style.borderRadius = inputBorderRadius;
        input.style.border = inputBorder;
        input.style.padding = inputPadding;
        input.style.height = inputHeight;
        input.style.width = inputWidth;
        input.style.color = inputColor;
        input.style.background = inputBackground;

        btn.style.borderRadius = btnBorderRadius;
        btn.style.border = btnBorder;
        btn.style.padding = btnPadding;
        btn.style.background = btnBackground;
        btn.style.color = btnColor;
        btn.style.height = btnHeight;
        btn.style.width = btnWidth;

    }, false);
    initVue();
})(window, Vue);










